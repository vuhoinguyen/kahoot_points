import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import withSizes from 'react-sizes';
import AppDrawer from './components/AppDrawer';
import AppContent from './components/AppContent';
import AppBar from './components/AppBar';
import SettingsDialog from './components/SettingsDialog';
import './App.css';
import setupApp from './setupApp';

const newtheme = createMuiTheme({
  palette: setupApp.palette,
  typography: setupApp.typography,
});

const styles = theme => ({
  root: {
    display: 'flex',
    overflow: 'hidden',
  },
});  

const screenSize = ({ width }) => ({
  isMobile: width < 480,
})

class App extends Component {
  // <Item name>: [current qty, current bonus, current score, per item score, bonus qty, bonus score]
  constructor(props) {
    super(props);
    this.state = {
      items: {
        'A': [0, 0, 0, 50, 3, 50],
        'B': [0, 0, 0, 30, 2, 30],
        'C': [0, 0, 0, 20, 0, 0],
        'D': [0, 0, 0, 30, 0, 0],
        'E': [0, 0, 0, 40, 5, 100],
        'F': [0, 0, 0, 50, 5, 150],
      },
      dialogOpen: false,
    };
  }

  handleButtonClick(i) {
    const items = this.state.items;
    const qty = items[i][0] + 1;
    const bonus = (items[i][4] === 0)? 0 : Math.floor(qty/items[i][4]) * items[i][5];
    const base_score = (qty*items[i][3]);
    items[i][0] = qty;
    items[i][1] = bonus;
    items[i][2] = base_score + bonus;
    this.setState({
      items: items,
    })
  }

  handleNewgameClick() {
    const items = this.state.items;
    Object.keys(this.state.items).forEach(key => {
      items[key][0] = 0;
      items[key][1] = 0;
      items[key][2] = 0;
    });
    this.setState({
      items: items,
    })
  }
  handleDialogClickOpen = () => {
    this.setState({
      dialogOpen: true,
    });
  };
  
  handleDialogClose = () => {
    this.setState({ dialogOpen: false });
  };

  handleSettingsChange (key, index, event) {
      const items = this.state.items;
      const value = parseInt(event.target.value, 10);
      items[key][index] = ((index === 4 && value < 0) || isNaN(value)) ? 0 : value;
      this.setState({
          items: items
      });
  }

  handleAddItem(item) {
    const items = this.state.items;
    if (item[0].trim() === '') return;
    const pntPerItem = (item[1] < 0 || isNaN(item[1])) ? 0 : parseInt(item[1]);
    const bonusQty = (item[2] < 0 || isNaN(item[2])) ? 0 : parseInt(item[2]);
    const bonusPnt = (item[3] < 0 || isNaN(item[3])) ? 0 : parseInt(item[3]);
    items[item[0]] = [0, 0, 0, pntPerItem, bonusQty, bonusPnt];
    this.setState({
      items: items
    })
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar handleDialogClickOpen={this.handleDialogClickOpen.bind(this)} theme={newtheme}/>
        <AppContent items={this.state.items} handleButtonClick={this.handleButtonClick.bind(this)} theme={newtheme} isMobile={this.props.isMobile} />
        <AppDrawer items={this.state.items} handleNewgameClick={this.handleNewgameClick.bind(this)} theme={newtheme} isMobile={this.props.isMobile} />
        <SettingsDialog
          items={this.state.items}
          theme={newtheme}
          open={this.state.dialogOpen}
          onClose={this.handleDialogClose.bind(this)}
          handleSettingsChange={this.handleSettingsChange.bind(this)}
          handleAddItem={this.handleAddItem.bind(this)}
        />
      </div>
    );
  }
}

export default withSizes(screenSize)(withStyles(styles)(App));