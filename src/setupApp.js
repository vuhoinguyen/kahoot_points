const AppConfig = {
    drawerWidth: '25%',
    palette: {
        primary: {
          main: '#46178f',
        },
        secondary: {
          main: '#26890c',
        },
    },
    typography: {
        useNextVariants: true,
    },
};
export default AppConfig;