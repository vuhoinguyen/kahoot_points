import AppConfig from '../setupApp';
import { createMuiTheme } from '@material-ui/core/styles';

const localStorageMock = {
    getItem: jest.fn(),
    setItem: jest.fn(),
    removeItem: jest.fn(),
    clear: jest.fn(),
  };
const items = {
    'A': [0, 0, 0, 50, 3, 50],
    'B': [0, 0, 0, 30, 2, 30],
    'C': [0, 0, 0, 20, 0, 0],
    'D': [0, 0, 0, 30, 0, 0],
    'E': [0, 0, 0, 40, 5, 100],
    'F': [0, 0, 0, 50, 5, 150],
}
const theme = createMuiTheme({
  palette: AppConfig.palette,
  typography: AppConfig.typography,
});
global.localStorage = localStorageMock;
global.items = items;
global.theme = theme;