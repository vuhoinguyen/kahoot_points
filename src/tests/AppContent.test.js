import React from 'react';
import ReactDOM from 'react-dom';
import AppContent from '../components/AppContent';
import AppConfig from '../setupApp';
import { createMuiTheme } from '@material-ui/core/styles';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AppContent items={global.items}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});