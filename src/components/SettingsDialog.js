import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Input from '@material-ui/core/Input';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Button } from '@material-ui/core';
import { MuiThemeProvider } from '@material-ui/core/styles';

const styles = theme => ({
    rules: {
        paddingLeft: '20px',
        paddingRight: '20px',
    },
    fab: {
        margin: '10px',
    },
    input: {
        margin: theme.spacing.unit,
    },
    expanel: {
        background: '#eee',
    },
});

class SettingsDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newItem: ['', 0, 0, 0],
        };
    }

    handleChange (i, event) {
        var value = event.target.value;
        if ((i > 0 && isNaN(value)))
            return;
        else if ( i > 0 &&  value.trim() === '')
            value = 0;
        const item = this.state.newItem;
        item[i] = (i === 2 && value < 0) ? 0 : value;
        this.setState({
            newItem: item,
        });
    }

    handleAddItem() {
        const item = this.state.newItem;
        if (item[0].trim() !== '')
            this.props.handleAddItem(item);
    }

    handleClose() {
        this.setState({
            newItem: ['', 0, 0, 0],
        });
        this.props.onClose();
    }

    render() {
        const { classes } = this.props;

        return (
            <Dialog
                open={this.props.open}
                onClose={() => this.handleClose()}
                aria-labelledby="settings-dialog-title"
                fullWidth maxWidth='lg'
            >
                <DialogTitle id="settings-dialog-title">Game Settings</DialogTitle>
                <div>
                    <Typography align='justify' className={classes.rules}>
                        This calculates the total points awarded to a player based on number of items
                        they have collected in a game. (A, B, C, and so on). Our items are scored individually. <br/>
                        Some items are worth more if collected in multiples: collect n of them,
                        and you’ll get y points.<br />
                        For example, item ‘A’ might be worth 50 points individually, but this
                        week we have a special bonus: collect three ‘A’s and you will get 50 points bonus
                        (total 200 instead of 150 points). <br/>
                        <b>EDIT:</b> You can edit the numbers (Points per item, Bonus quantity and Bonus points) directly here.
                        The numbers can be negative, except for the "Bonus quantity".
                        If a negative quantity is given, it's counted as 0. <br />
                        <b>ADD:</b> You can add more items by expanding the form at the bottom. Rules are the same as for EDIT
                        (Quantity can't be negative). If a duplicate Item name is given, that item is updated with the new values.
                        <br />
                        Enjoy!!
                    </Typography>

                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Item</TableCell>
                                <TableCell align="right">Points per item</TableCell>
                                <TableCell align="right">Bonus quantity</TableCell>
                                <TableCell align="right">Bonus points</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {Object.entries(this.props.items).map(([key, item]) => (
                            <TableRow key={key}>
                                <TableCell component="th" scope="row">
                                    {key}
                                </TableCell>
                                <TableCell align="right">
                                    <Input
                                        id={key}
                                        type='number'
                                        placeholder={item[3].toString()}
                                        onChange={event => this.props.handleSettingsChange(key, 3, event)}
                                    />
                                </TableCell>
                                <TableCell align="right">
                                    <Input
                                        id={key}
                                        type='number'
                                        placeholder={item[4].toString()}
                                        onChange={event => this.props.handleSettingsChange(key, 4, event)}
                                        inputProps={{ min: "0" }}
                                    />
                                </TableCell>
                                <TableCell align="right">
                                    <Input
                                        id={key}
                                        type='number'
                                        placeholder={item[5].toString()}
                                        onChange={event => this.props.handleSettingsChange(key, 5, event)}
                                    />
                                </TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
                    <ExpansionPanel className={classes.expanel}>
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.heading}>Add item (Click to expand)</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                        <Input
                            placeholder="Item name"
                            className={classes.input}
                            fullWidth
                            onChange={event => this.handleChange(0, event)}
                        />
                        <Input
                            placeholder="Points per item"
                            className={classes.input}
                            type='number'
                            fullWidth
                            onChange={event => this.handleChange(1, event)}
                        />
                        <Input
                            placeholder="Bonus quantity"
                            className={classes.input}
                            type='number'
                            fullWidth
                            inputProps={{ min: "0" }}
                            onChange={event => this.handleChange(2, event)}
                        />
                        <Input
                            placeholder="Bonus points"
                            className={classes.input}
                            type='number'
                            fullWidth
                            onChange={event => this.handleChange(3, event)}
                        />
                        <MuiThemeProvider theme={this.props.theme}>
                            <Button variant="contained" color="primary" onClick={() => this.handleAddItem()}>Add</Button>
                        </MuiThemeProvider>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </div>
            </Dialog>
        );
    }
}

SettingsDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    onClose: PropTypes.func,
};

export default withStyles(styles)(SettingsDialog);