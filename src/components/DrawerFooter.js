import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { MuiThemeProvider } from '@material-ui/core/styles';

const styles = theme => ({
    drawerFooter: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
        whiteSpace: 'nowrap',
        padding: theme.spacing.unit * 3,
        height: 120,
        flexShrink: 0,
    },
    drawerFooterMobile: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
        whiteSpace: 'nowrap',
        padding: theme.spacing.unit * 1,
        height: 120,
        flexShrink: 0,
    },
    newGame: {
        marginLeft: theme.spacing.unit * 3,
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
        fontSize: 15,
    },
    newGameMobile: {
        fontSize: 7,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    fullHeight: {
        height: '100%',
    },
    totalText: {
        fontSize: 15,
    },
    totalValue: {
        fontSize: 30,
        color: '#f00',
    }
}); 

function DrawerFooter(props) {
    const { classes } = props;
    var newGameStyle = classes.newGame;
    if (props.isMobile) newGameStyle = classes.newGameMobile;
    var drawerFooterStyle = classes.drawerFooter;
    if (props.isMobile) drawerFooterStyle = classes.drawerFooterMobile;
    return (
        <Grid container direction="row" alignItems="center" alignContent="center" className={drawerFooterStyle}>
            <Grid item xs>
            <Grid container direction="column" justify="center" alignItems="stretch" className={classes.fullHeight}>
                    <Grid item xs className={classes.totalText}>TOTAL</Grid>
                    <Grid item xs className={classes.totalValue}>{props.sum(props.items, 2)}</Grid>
            </Grid>
            </Grid>

            <Grid item xs>
                <MuiThemeProvider theme={props.theme}>
                <Button variant="contained" color="primary" className={newGameStyle} onClick={() => props.handleNewgameClick()} style={{marginLeft: 'auto', marginRight: 'auto'}}>NEW GAME</Button>
                </MuiThemeProvider>
            </Grid>
        </Grid>
    )
}

export default withStyles(styles)(DrawerFooter);
