import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import SettingsIcon from '@material-ui/icons/Settings';
import Fab from '@material-ui/core/Fab';
import { MuiThemeProvider } from '@material-ui/core/styles';
import kahoot_points from '../img/kahoot_points.svg';
import { withStyles } from '@material-ui/core/styles';
import setupApp from '../setupApp';

const drawerWidth = setupApp.drawerWidth;
const styles = theme => ({
    appBar: {
        width: `calc(100% - ${drawerWidth})`,
        marginRight: drawerWidth,
        backgroundColor: '#46178f',
    },
    fab: {
        marginLeft: 'auto',
    }
});

function KahootTestAppBar(props) {
    const { classes } = props;
    return (
        <AppBar position="fixed" className={classes.appBar}>
            <MuiThemeProvider theme={props.theme}>
                <Toolbar>
                    <img src={kahoot_points} alt="Kahoot!" className="header_item"/>
                    <Fab
                        variant="extended"
                        size="small"
                        color="secondary"
                        aria-label="Add"
                        className={classes.fab}
                    >
                        <SettingsIcon onClick={() => props.handleDialogClickOpen()}/>
                    </Fab>
                </Toolbar>
            </MuiThemeProvider>
        </AppBar>
    )
}

export default withStyles(styles)(KahootTestAppBar);