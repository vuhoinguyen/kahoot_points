import React, { Component } from 'react';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/core/styles';
import setupApp from '../setupApp';
import DrawerItemList from './DrawerItemList';
import DrawerFooter from './DrawerFooter';

const drawerWidth = setupApp.drawerWidth;

const styles = theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        width: '100%',
        textAlign: 'center',
        color: theme.palette.text.secondary,
        whiteSpace: 'nowrap',
        height: '64px',
        padding: theme.spacing.unit * 3,
    },
    bonusText: {
        textAlign: 'center',
        padding: theme.spacing.unit,
        fontSize: 15,
    },
});

class KahootTestDrawer extends Component { 
    sum(items, col) {
      var sum = 0;
      for (var i in items)
        sum = sum + items[i][col];
      return sum;
    }

    render() {
        const { classes } = this.props;
        return (
        <Drawer className={classes.drawer} variant="permanent" classes={{ paper: classes.drawerPaper }} anchor="right">
            <div className={classes.drawerHeader}> {this.props.isMobile? 'ITEMS' : 'PLAYER ITEMS'} </div>
            <Divider />

            <DrawerItemList items={this.props.items} isMobile={this.props.isMobile}/>
            <Divider />

            <div className={classes.bonusText}>BONUS: {this.sum(this.props.items, 1)}</div>
            <Divider />

            <DrawerFooter
                items={this.props.items}
                theme={this.props.theme}
                handleNewgameClick={() => this.props.handleNewgameClick()}
                sum={this.sum}
                isMobile={this.props.isMobile}
            />
        </Drawer>
        )
    }
}

export default withStyles(styles)(KahootTestDrawer);