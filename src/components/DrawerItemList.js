import React from 'react';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    itemRow: {
        width: '100%',
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
        textAlign: 'center',
        fontSize: 'small',
    },
    itemRowMobile: {
        width: '100%',
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
        textAlign: 'center',
        fontSize: 'xx-small',
    },
});

function DrawerItemList(props) {
    const { classes } = props;
    var itemRowStyle = classes.itemRow;
    if (props.isMobile) itemRowStyle = classes.itemRowMobile;

    return (
        <List style={{maxHeight: '100%', overflow: 'auto', flex: 1}}>
            <ListItem>
                <Grid className={itemRowStyle} container>
                    <Grid item xs><b>Item</b></Grid>
                    <Grid item xs><b>Qty</b></Grid>
                    <Grid  item xs><b>Score</b></Grid>
                </Grid>
            </ListItem>
            {Object.entries(props.items).map(([key, item]) => (
                item[0] !== 0 ? (
                    <ListItem button key={Math.random().toString(36).substr(2, 5)}>
                        <Grid key={Math.random().toString(36).substr(2, 5)} className={itemRowStyle} container>
                        <Grid key={Math.random().toString(36).substr(2, 5)} item xs>{key}</Grid>
                        <Grid key={Math.random().toString(36).substr(2, 5)} item xs>{item[0]}</Grid>
                        <Grid key={Math.random().toString(36).substr(2, 5)} item xs>{item[2]}</Grid>
                        </Grid>
                    </ListItem>
                ) : (
                    <div key={key}></div>
                )
            ))}
        </List>
    )
}

export default withStyles(styles)(DrawerItemList);
