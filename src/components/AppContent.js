import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { MuiThemeProvider } from '@material-ui/core/styles';

const styles = theme => ({
    toolbar: theme.mixins.toolbar,
    appBody: {
        marginTop: '10px',
        paddingLeft: '10px',
        paddingRight: '10px',
      },
    button: {
        textAlign: 'center',
        fontSize: '30',
        color: theme.palette.text.secondary,
        padding: theme.spacing.unit*4,
        width: '90%',
        minHeight: '200px',
    },
    buttonGrid: {
        textAlign: 'center',
        width: '100%',
        height: '100%',
    },
    instruction: {
        marginLeft: theme.spacing.unit*4,
        marginTop: theme.spacing.unit*3,
    }
}); 

function KahootTestContent(props) {
    const { classes } = props;
    return (
        <main className={classes.content} style={{maxHeight: '100vh', overflow: 'hidden', flex: 1}}>
            <MuiThemeProvider theme={props.theme}>
                <div className={classes.toolbar} />
                <div className={classes.instruction}> Click on items below to collect... </div>
                <Grid
                    container
                    justify="flex-start"
                    alignContent="flex-start"
                    alignItems="flex-start"
                    spacing={24}
                    className={classes.appBody}
                    style={{maxHeight: '85vh', overflow: 'auto', flex: 1}}
                >
                    {Object.keys(props.items).map(key => (
                    <Grid item xs={4} key={key} className={classes.buttonGrid}>
                        <Button
                        variant="outlined"
                        color="secondary"
                        className={classes.button}
                        onClick={() => props.handleButtonClick(key)}
                        >
                        {key}
                        </Button>
                    </Grid>
                    ))}
                </Grid>
            </MuiThemeProvider>
        </main>
    )
}

export default withStyles(styles)(KahootTestContent);